from tkinter import *

"""
Création de la classe Interfaces
"""
class Interfaces:

    """
    Cette fonction permet de créer la fenêtre d'affichage
    """
    def __init__ (self):
        self.fenetre = Tk()
        self.fenetre.geometry ( "950x600")

        """
        Cette fonction permet de mettre l'image sr la fenêtre que l'on a crée
        """
        self.photo = PhotoImage(file = "chevre.png")
        self.canvas = Canvas(self.fenetre,width=200, height=177)
        self.canvas.create_image(0, 0, anchor=NW, image=self.photo)
        self.canvas.pack (side="right", fill = "both", padx = 5, pady = 5)

        """
        Cette fonction permet de créer le menu 'règles'
        """
        self.menubar = Menu(self.fenetre)

        self.menu1 = Menu(self.menubar, tearoff=0)
        self.menu1.add_command(label="Lister", command= 0)
        self.menu1.add_command(label="Créer", command= 0)
        self.menubar.add_cascade(label="Règles", menu=self.menu1)

        """
        Cette fonction permet de créer dans la barre de menu l'onglet '?'
        """
        self.menu2 = Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label="?", menu=self.menu2)
        self.fenetre.config(menu=self.menubar)

        """
        Cette fonction permet de créer le frame 'Renommer en Lots'
        """
        self.Frame1 = Frame(self.fenetre, borderwidth=0, relief=GROOVE)
        self.Frame1.place(x=425, y=20)

        self.label_1= Label(self.Frame1, text="Renommer en Lots")
        self.label_1.pack()

        """
        Cette fonction permet de créer le framme 'nom du répertoire'
        """
        self.Frame2 = Frame(self.fenetre, borderwidth=2, relief=GROOVE)
        self.Frame2.place(x=300, y=100)
        self.label_2= Label(self.Frame2, text="Nom du répertoire")
        self.label_2.pack(side="left")

        self.entre_nom_rep = Entry (self.Frame2, width=20)
        self.entre_nom_rep.pack(side="right")

        """
        Cette fonction permet de créer le frame 'amorce'
        """
        self.frame3 = Frame(self.fenetre,borderwidth=0, relief=GROOVE)
        self.frame3.place(x=50, y=300)
        self.label3 = Label(self.frame3, text="Amorce")
        self.label3.pack(side="top", fill="both")
        self.optionListe3 = ("Aucun","Chiffre","Lettre")
        self.stringAmort = StringVar()
        self.stringAmort.set(self.optionListe3[0])
        self.listMenu3 = OptionMenu(self.frame3, self.stringAmort, *self.optionListe3)
        self.listMenu3.pack(side="bottom", fill="both")

        """
        Cette fonction permt de créer le bouton 'préfixe'
        """

        self.Frame4 = Frame(self.fenetre, borderwidth=0, relief=GROOVE)
        self.Frame4.place(x=200, y=300)
        self.label_4= Label(self.Frame4, text="Préfixe")
        self.label_4.pack(side="top")

        self.entre_nom_rep = Entry (self.Frame4, width=20)
        self.entre_nom_rep.pack(side="top")

        """
        Cette fonction permet de créer le frame 'postfixe'
        """
        self.Frame5 = Frame(self.fenetre, borderwidth=0, relief=GROOVE)
        self.Frame5.place(x=600, y=300)
        self.label_5= Label(self.Frame5, text="Postfixe")
        self.label_5.pack(side="top")

        self.entre_nom_rep = Entry (self.Frame5, width=20)
        self.entre_nom_rep.pack(side="top")

        """
        Cette fonction permet de créer le frame 'Extenson concernée'
        """
        self.Frame6 = Frame(self.fenetre, borderwidth=0, relief=GROOVE)
        self.Frame6.place(x=800, y=300)
        self.label_6= Label(self.Frame6, text="Extension concernée")
        self.label_6.pack(side="top")

        self.entre_nom_rep = Entry (self.Frame6, width=20)
        self.entre_nom_rep.pack(side="top")

        """
        Cette fonction permet de créer le bouton coché 'Nom du fichier', 'Nom original'
        """
        self.frame_cocher = Frame(self.fenetre, borderwidth=0, relief=GROOVE)
        self.frame_cocher.place(x=350, y=300)
        self.label_cocher = Label(self.frame_cocher, text="Nom du fichier")
        self.label_cocher.pack(side="top", fill="both")
        self.valeur_cocher = StringVar()
        self.valeur_cocher_saisis = StringVar()
        self.choix1 = Radiobutton(self.frame_cocher, text="Nom Original", variable=self.valeur_cocher, value=1)
        self.choix1.pack()
        self.choix1.select()
        self.frame_cocher_saisis= Frame(self.frame_cocher, borderwidth=0, relief=GROOVE)
        self.frame_cocher_saisis.pack(side=LEFT, padx=5,pady=5)
        self.choix2 = Radiobutton(self.frame_cocher_saisis, variable=self.valeur_cocher, value=2)
        self.Choix_saisis = Entry(self.frame_cocher_saisis, textvariable=self.valeur_cocher_saisis)
        self.choix2.pack(side=LEFT)
        self.Choix_saisis.pack(side=RIGHT)

        """
        Cette fonction permet de créer le bouton 'Renommer'
        """
        self.bouton_renommer = Button(self.fenetre, text="Renommer", command=self.fenetre.quit)
        self.bouton_renommer.place(x = 800, y = 500)

        """
        Cette fonction permet de créer le frame 'apartir de'
        """
        self.Frame7 = Frame(self.fenetre, borderwidth=0, relief=GROOVE)
        self.Frame7.place(x=20, y=500)
        self.label_7= Label(self.Frame7, text="A partir de")
        self.label_7.pack(side="top")

        self.entre_nom_rep = Entry (self.Frame7, width=20)
        self.entre_nom_rep.pack(side="top")









