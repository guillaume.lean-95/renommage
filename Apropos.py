import sys
sys.dont_write_bytecode = True
from tkinter import *


class Apropos:
    """
    Cette fonction permet de créer la classe constructeur
    """
    def __init__(self):
        self.root = Toplevel()
        self.root.title("A propos")
        self.fenetre = 300
        self.fenetre2 = 300
        self.s1 = self.root.winfo_screenwidth()
        self.s2 = self.root.winfo_screenheight()
        self.root.geometry((self.fenetre, self.fenetre2, (self.s1-self.fenetre)/2, (self.s2-self.fenetre2)/2))
        self.photo = PhotoImage(file="../img/chevre.png")
        self.canvas =  Canvas(self.root, width=260, height=189)   
        self.canvas.create_image(0, 0, anchor=NW, image=self.photo)
        self.canvas.pack(pady=10)
        self.label = Label(self.root, text=("Logiciel de renommage LEAN Guillaume")
        self.label.pack()
        self.root.mainloop()

