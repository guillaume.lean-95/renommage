from tkinter import *
from ListeRegle import *
from Regle import *
import Interface as inter

"""
Création de la classe 'Lister'
"""
class Lister:
    """
   Création de la méthode constructeur
    """
    def __init__(self):
        self.liste_regle = ListeRegle()
        self.liste_regle.regles[:] = []
        self.liste_regle.charger()
        
        """
        Création de la fenêtre
        """
        self.root = Tk()
        self.root.title("Liste de règle")
        self.fenetre1 = 550
        self.fenetre2 = 300
        self.root.resizable(width=False, height=False)
        self.s1 = self.root.winfo_screenwidth()
        self.s2 = self.root.winfo_screenheight()
        self.root.geometry("%dx%d+%d+%d" % (self.fenetre1, self.fenetre2, (self.s1-self.fenetre1)/2, (self.s2-self.fenetre2)/2))

        """
        Label titre principal
        """
        self.label_principal = Label(self.root,text="Liste des Règles", font=("Helvetica",14))
        self.label_principal.pack()

        """
        Création d'une listebox
        """
        self.Liste = Frame(self.root, borderwidth=0, relief=GROOVE)
        self.Liste.place(x=23, y=48)
        self.scro = Scrollbar(self.Liste)
        self.scro.pack(side=RIGHT, fill=Y)
        self.BoxRegle = Listbox(self.frameListe, width=85)
        self.listeBoxRegle.pack()
        self.scro.config(command=self.listeBoxRegle.yview)
        self.BoxRegle.insert(0, "Amorce| A partir de | Préfixe | Nom Fichier | Postfixe | Extensions")
        self.disable_item("0")

        """
        Création bouton Utiliser
        """
        self.boutonUtiliser = Button(self.root, width=73, text="Utiliser", command=self.utiliser)
        self.boutonUtiliser.place(x=23, y=240)

        """
        Ajout des règles
        """
        self.ajouterRegles()

    """
    Création de la méthode pour insérer des règles
    """
    def ajouter_Regles(self):
        i= 1
        for regle in self.liste_regle.regles:
            self.BoxRegle.insert(i, "regle.amorce+" |"+regle.apartirde+"|"+regle.prefixe+" | "+str(regle.nomfichier)+"|"+regle.postfixe+"|"+str(regle.extensions))
            i+= 1
        return True
    
    
    def utiliser(self):
        if self.BoxRegle.curselection():
            index = self.BoxRegle.curselection()[0]
            regle_string = self.BoxRegle.get(index)
            liste_regle = regle_string.split("|")
            amorce = liste_regle[0].strip()
            apartirde = liste_regle[1].strip()
            prefixe = liste_regle[2].strip()
            nom_fichier = liste_regle[3].strip()
            if nom_fichier == "True":
                nom_fichier = True
            postfixe = liste_regle[4].strip()
            extensions = liste_regle[5].strip()[1:-1]
            stri_Extensions =""
            for extension in extensions.split(","):
                stri_extentensions += extension.strip()[2:-1]+","
            extensions = stri_extensions[:-1]
            if extensions == "*":
                extensions = ""
            self.root.destroy()
            main = inter.interface(amorce,apartirde,prefixe,nom_fichier,postfixe,extensions)
            return True
        else:
            messagebox.showerror("Error")
            return False
        
