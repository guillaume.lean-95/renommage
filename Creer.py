from tkinter import *
from ListeRegle import *
from Regle import *
import Interface as inter

class Creer:
    """
    Création de la class constructeur
    """
    def __init__(self):
        """
        Création de la fenêtre
        """
        self.root = Tk()
        self.root.title("Création de règle")
        self.fenetre = 300
        self.fenetre2 = 450
        self.s1 = self.root.winfo_screenwidth()
        self.s2 = self.root.winfo_screenheight()
        self.root.resizable(width=False, height=False)
        self.root.geometry (self.fenrw, self.fenrh, (self.sw-self.fenrw)/2, (self.sh-self.fenrh)/2))
        
        """
       Création du label principal
        """
        self.labelMain = Label(self.root,text="Création de Règle", font=("Helvetica",12))
        self.labelMain.pack()

        """
       création de la fram amorce
        """
        self.Amorce = Frame(self.root,borderwidth=0, relief=GROOVE)
        self.Amorce.place(x=35, y=42)
        self.label_Amorce = Label(self.Amorce, text="Amorce")
        self.label_Amorce.pack(side="left", fill="both")
        self.option_Amorce = ("Aucun","Chiffre","Lettre")
        self.string = StringVar()
        self.string.set(self.option_Amorce[0])
        self.Menu_Amorce = Option(self.Amorce, self.string, *self.option_Amorce)
        self.Menu_Amorce.pack(side="right", fill="both",padx=50)


        """
       création du frame 'A partir de'
        """
        self.Apartirde = Frame(self.root, borderwidth=0, relief=GROOVE)
        self.Apartirde.place(x=20, y=100)
        self.label_Apartirde = Label(self.Apartirde, text="A partir de")
        self.label_Apartirde.pack(side="left", fill="both")
        self.saisie_Apartirde = Entry(self.Apartirde, width=11)
        self.saisie_Apartirde.pack(side="right",fill="both",padx=29)


        """
        Création du frame 'Préfix'
        """
        self.Prefixe = Frame(self.root, borderwidth=0,relief=GROOVE)
        self.Prefixe.place(x=20, y=150)
        self.label_Prefixe = Label(self.Prefixe, text="Préfixe")
        self.label_Prefixe.pack(side="left", fill="both")
        self.saisie_Prefixe = Entry(self.Prefixe, width=20)
        self.saisie_Prefixe.pack(side="right", fill="both", padx=60)

        """
      création du bouton 'nom de fichier'
        """
        self.Nom = Frame(self.root, borderwidth=0, relief=GROOVE)
        self.Nom.place(x=20, y=200)
        self.label_Nom = Label(self.Nom, text="Nom du fichier")
        self.label_Nom.pack(side="left", fill="both")
        self.val_Nom = StringVar()
        self.val_Nom2 = StringVar()
        self.choix1 = Radiobutton(self.Nom, text="Nom Original", variable=self.val_Nom, value=1)
        self.choix1.pack()
        self.choix1.select()
        self.saisie= Frame(self.Nom, borderwidth=0, relief=GROOVE)
        self.saisie.pack(side=LEFT, padx=5,pady=5)
        self.choix2 = Radiobutton(self.saisie, variable=self.Nom, value=2)
        self.saisie2 = Entry(self.saisie, textvariable=self.val_Nom2)
        self.choix2.pack(side=LEFT)
        self.saisie2.pack(side=RIGHT)

        """
        Création frame 'postfixe'
        """
        self.Postfixe = Frame(self.root, borderwidth=0, relief=GROOVE)
        self.Postfixe.place(x=15,y=320)
        self.label_Pf = Label(self.Postfixe, text="Postfixe")
        self.label_pf.pack(side="left", fill="both")
        self.saisie_pf = Entry(self.Postfixe, width=20)
        self.saisie_pf.pack(side="right", fill="both", padx=60)

        """
        Création du frame 'extension'
        """
        self.Extension = Frame(self.root, borderwidth=0, relief=GROOVE)
        self.Extension.place(x=20, y=350)
        self.label_ext = Label(self.Extension, text="Extensions")
        self.label_ext.pack(side="left", fill="both")
        self.saisie_ext = Entry(self.Extension, width=20)
        self.saisie_ext.pack(side="right", fill="both",padx=60)

        """
        Définition du bouton
        """
        self.bouton = Button(self.root, text="Créer", width=35,command=self.clic)
        self.bouton.place(x=25, y=350)

    """
   Cette fonction permet de créer une règle
    """
    def clic(self):
        amorce = self.stringAmort.get()
        apartirde = self.saisieApartirde.get()
        prefix = self.saisiePrefixe.get()
        if len(apartirde) <= 3 and ((amorce == "Chiffre" and (apartirde.isnumeric() or apartirde =="")) or (amorce=="Lettre" and (apartirde.isalpha() or apartirde ==""))) or amorce=="Aucun":
            if self.Nom.get() == "1":
                nom_fichier = True
            if self.Nom.get() == "2":
                nom_fichier = self.val_nom2.get()
            postfixe = self.saisie_pf.get()
            extensions = self.saisie_ext.get()
            stri = "Amorce: "+amorce+" A partir de : " + apartirde + "Prefix : "+ prefix + "Nom Fichier : " + str(nom_fichier)+"Postfixe: " + postfixe +"Extensions: " + extensions
            question = messagebox.askyesno("Confirmez les attributs :" + stri, icon="question")
            if question == True:
                liste_regle = ListeRegle()
                liste_regle.regles[:] = []
                liste_regle.charger()
                regle = Regle(amorce,apartirde,prefix, nomFichier,postfixe)
                for i in extensions.split(','):
                    regle.ajoutExtension("*"+i)
                liste_regle.ajouteRegle(regle)
                liste_regle.sauvegarder()
                re.extensions[:] = []
                messagebox.showinfo("Création de la règle","Règle créée avec succès !")
                self.root.destroy()
                inter.Interface(amorce, apartirde, prefix,nom_fichier,postfixe,extensions)
            else:
                return
        else:
            messagebox.showerror("Error")
            return
