import sys


"""
Création de la classe Regle
"""
class Regle:
    """
    Création du constructeur
    """
    def __init__(self, amorce,apartirde,prefixe,nom_fichier,postfixe,extensions):
        self.amorce = amorce
        self.apartirde= apartirde
        self.prefixe = prefixe
        self.nomfichier = nom_fichier
        self.postfixe = postfixe
        self.extensions = extensions

    """
   Création de la méthode string
    """
    def __str__(self):
        return "Le fichier a été renommé en."

    """
    Création de la méthode Getter
    """    
    def _get_amorce(self):
        return self.amorce
    
    """
    Création de la méthode set
    """
    def _set_amorce(self, a):
        try:
            self.amorce = a
            return True
        except ValueError:
            return "Error"
    
    """
    Création du Getter
    """  
    def _get_apartirde(self):
        return self.apartirde

    """
    Création du setter
    """
    def _set_apartirde(self, apartirde):
        try:
            self.apartirde = aapartirde
            return True
        except ValueError:
            return "Error"

    """
    Création du getter
    """         
    def _get_prefix(self):
        return self.prefixe

    """
    Création du setter
    """
    def _set_prefix(self, p):
        try:
            self.prefixe = p
            return True
        except ValueError:
            return "Error"
    
    """
   Création du getter
    """  
    def _get_nomFichier(self):
        return self.nomfichier

    """
    Création du setter
    """
    def _set_nomFichier(self,n):
        try:
            self.nomfichier = n
            return True
        except ValueError:
            return "Error"

    """
    Création du getter
    """
    def _get_postfixe(self):
        return self.postfixe
    
    """
   Création du setter
    """
    def _set_postfixe(self,p):
        try:
            self.postfixe = p
            return True
        except ValueError:
            return "Error"

    """
    Création du getter
    """  
    def _get_extensions(self):
        return self.extensions
    
    """
    Création du setter
    """
    def _set_extensions(self,e):
        try:
            self.extensions = e
            return True
        except ValueError:
            return "Error"

    """
    Création de la méthode pour ajouter des extensions
    """
    def ajoutExtension(self,ext):
        self.extensions.append(x)
        return True