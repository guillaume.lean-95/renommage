from tkinter import *
import Lister import *
import Renommage import *
import interface import *

"""
Création de la classe simulation
"""
class Simulation:
    """
    Création de la classe constructeur
    """
    def __init__(self,message,dictionnaire,nom_repertoirer,regle,extensions):
        self.message = message
        self.dictionnaire = dictionnaire
        self.regle = regle
        self.nom_Repertoire = nom_repertoire
        self.extensions = extensions

        self.root = Tk()
        self.root.title("Simulation")
        self.fenetre = 700
        self.fenetre2 = 500
        self.s1 = self.root.winfo_screenwidth()
        self.s2 = self.root.winfo_screenheight()
        self.root.resizable(width=False, height=False)
        self.root.geometry("%dx%d+%d+%d" % (self.fenetre, self.fenetre2, (self.s1-self.fenetre)/2, (self.s2-self.fenetre2)/2))

        self.texte = Frame(self.root, borderwidth=0, relief=GROOVE)
        self.texte.place(x=5, y=5)
        self.position = Text(self.texte,width=92, height=30)
        self.position.pack(pady=5,padx=5,side=LEFT, fill=Y)
        self.scro = Scrollbar(self.frameZoneTxt)
        self.scro.pack(side=RIGHT, fill=Y)
        self.scro.config(command=self.txtZone.yview)
        self.position.config(yscrollcommand=self.scroll.set)
        self.position.insert(INSERT,self.message)

        self.Bouton = Frame(self.root,borderwidth=0,relief=GROOVE)
        self.Bouton.place(x=15,y=550)
        self.Non = Button(self.Bouton, text="Non, retourner à l'interface", command=self.Bouton_non)
        self.Non.pack(side=LEFT,fill="both")
        self.Oui = Button(self.Bouton, text="renommer", command=self.Bouton_oui)
        self.Oui.pack(side=RIGHT,fill="both",padx=500)

    def Bouton_non(self):
        self.root.destroy()
        striExten =""
        for extension in self.extensions.split(","):
          

    def Bouton_oui(self):
        renomme = rename.Renommage(self.nom_Repertoire, self.regle)
        renomme.renommer(self.message, self.dictionnaire)
        self.root.destroy()
        lister = li.Lister()
        return True

