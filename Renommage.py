from Action import *
from Regle import *


"""
Création de la classe renommage
"""
class Renommage(Action):
    """
    Création de la classe constructeur
    """
    def __init__(self,n,r):
        Action.__init__(self,n,r)
    
    """
    Création de la classe 'renomme'
    """
    def renomme(self,message,dict):
        for original, modif in dict.items():
            os.rename(original,modif)
            messagebox.showinfo("Les fichiers ont été renommés")
            return True
        else:
            messagebox.showerror("Error")
            return False
            
    """
   Cette fonction permet de créer le 'str'
    """
    def __str__(self):
        return "Classe de renommage du fichier"


