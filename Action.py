import sys
sys.dont_write_bytecode = True
import os
import tkinter
from tkinter import messagebox
from glob import glob
from os.path import join


"""
Création de la classe Action
"""
class Action:
    """
    Création de la classe conctructeur
    """
    def __init__(self,nom_Repertoire,regle):
        self.nom_Repertoire = nom_Repertoire
        self.regle = regle

    """
    Création de la méthode Getter
    """
    def _get_nom_Repertoire(self):
        return self.nom_Repertoire
    
    """
    Création de la méthode Setter
    """
    def _set_nom_Repertoire(self,n):
        try:
            self.nom_Repertoire = nom_Repertoire
            return True
        except ValueError:
            return "Error"

    """
    Création de la méthode Getter
    """
    def _get_regle(self):
        return self.regle
    
    """
   Création de la méthode Setter
    """
    def _set_regle(self,r):
        try:
            self.regle = r
            return True
        except ValueError:
            return "Error"
    
    """
    Création de la méthode string
    """
    def __str__(self):
        return "Le nom du répertoire est : "+self.nom_Repertoire+" et votre règle est : "+str(self.regle)

    
    """
    Création de la méthode simule
    """
    def simule(self):
        nom_original = ""
        extension = ""
        changer = ""
        prefixe = ""
        postfixe = ""
        nom_fichier = ""
        stri = ""
        fichiers = []
        dictRetour = {} 
        try:
            if self.regle.extensions != [""]:
                for x in self.regle.extensions:
                    fichiers.extend(glob(join(self.nom_Repertoire, x)))
                self.regle.extensions[:] = []
            else:
                fichiers[:] = []
                fichiers = os.list(self.nom_Repertoire)
            i=1
            if fichiers != "":
                amorce_suivante = ""
                for fichier in fichiers:
                    if os.path.isfile(fichier):
                        nom_original = os.path.basename(fichier).split(".")
                        repertoire = os.path.dirname(fichier)
                        extension = os.path.splitext(fichier)
                        stri += " - Nom fichier original : "+ nom_original + extension+" \n - Nouveau Nom : "
                        
                        if amorce_suivante == "":
                            amorce = self.regle.apartirde
                            amorce_suivante = amorce
                        else:
                            amorce = self.getAmorce(amorce_suivante, self.regle.amorce)
                            amorce_suivante = amorce
                        stri+=amorce
                        if self.regle.prefixe != "":
                            stri += self.regle.prefixe
                            prefixe = self.regle.prefixe
                            
                        if self.regle.nom_fichier != True:
                            stri += self.regle.nom_fichier+"_"+ str(i)
                            nomF = self.regle.nom_fichier+"_"+ str(i)
                            i+=1
                        else:
                            stri += original_nom
                            nom_fichier = original_nom                    

                        if self.regle.postfixe != "":
                            stri += self.regle.postfixe + extension+"\n"
                            post = self.regle.postfixe
                        else:
                            stri += extension+"\n"
                          
                        ancien_nom = repertoire + "/" + nom_original + extension
                        nouveau_nom = repertoire + "/" + amorce + prefixe + nom_fichier + postfixe + extension
                        dictRetour[ancien_nom] = nouveau_nom 

                if stri == "":
                    stri = "Error"
                return (stri,dictRetour)
            else:
                showinfo("pas de fichier ! ")
        except IOError:
            return "répertoire introuvable"

    """
    Création de la méthode amorce
    """
    def get_Amorce(self, apartirde,genre):
        apartirde = apartirde.upper()
        if apartirde == "a":
            genre = "Lettre"
        if apartirde == "000":
            genre = "Chiffre"
        if genre == "Aucun":
            amorce=""

        if genre == "Chiffre":
            if int(apartirde) == 999:
                amorce = "a"
            else:
                amorce =apartirde + 1

        if genre == "Lettre":
            amorce = apartirde
                                
            for x in range(len(apartirde)):
                if x == 0:
                    lettre = amorce[-i-1]
                else:
                    lettre = amorce[-i]
                if amorce != "ZZZ":
                    if lettre != "Z":
                        lettre = chr(int(ord(lettre)) + 1)
                        liste_amorce = list(amorce)

                        if i == 0:
                            liste_amorce[-i-1] = lettre
                        else:
                            liste_amorce[-i] = lettre
                        amorce = ""
                        for a in liste_amorce:
                            amorce +=a                       
                   